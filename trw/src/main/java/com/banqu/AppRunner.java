package com.banqu;

import java.util.concurrent.CompletableFuture;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.trw.service.TrwAsyncService;

@Component
@Profile("app_runner")
public class AppRunner implements CommandLineRunner {

    private static final Log logger = LogFactory.getLog(AppRunner.class);

    private final TrwAsyncService service;

    public AppRunner(TrwAsyncService service) {
        this.service = service;
    }

    @Override
    public void run(String... args) throws Exception {
        // Start the clock
        long start = System.currentTimeMillis();

        // Kick of multiple, asynchronous lookups
        CompletableFuture<Integer> page1 = service.getRandom(12345);
        CompletableFuture<Integer> page2 = service.getRandom(12345);
        CompletableFuture<Integer> page3 = service.getRandom(12345);

        // Wait until they are all done
        CompletableFuture.allOf(page1,page2,page3).join();

        // Print results, including elapsed time
        logger.info("Elapsed time: " + (System.currentTimeMillis() - start));
        logger.info("--> " + page1.get());
        logger.info("--> " + page2.get());
        logger.info("--> " + page3.get());

    }

}
