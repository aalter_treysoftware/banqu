package com.banqu;

import java.util.concurrent.Executor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;


@SpringBootApplication
@EnableAsync
//@EnableScheduling
public class Application {
    
    @Value("${rwave.thread.core-pool:3}")
    private int corePoolSize;

    @Value("${rwave.thread.max-pool:5}")
    private int maxPoolSize;

    @Value("${rwave.queue.capacity:500}")
    private int queueCapacity;

    @Value("${rwave.thread.timeout:2}")
    private int threadTimeout;
    
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean(name="threadPoolTaskExecutor")
    public Executor asyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(corePoolSize);
        executor.setMaxPoolSize(maxPoolSize);
        executor.setQueueCapacity(queueCapacity);
        executor.setKeepAliveSeconds(threadTimeout);
        executor.setThreadNamePrefix("TestAsynchService-");
        executor.initialize();
        return executor;
    }
    
    /*@Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {

            System.out.println("Let's inspect the beans provided by Spring Boot:");

            String[] beanNames = ctx.getBeanDefinitionNames();
            Arrays.sort(beanNames);
            for (String beanName : beanNames) {
                System.out.println(beanName);
            }

        };
    } */

}