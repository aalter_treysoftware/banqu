package com.banqu.controller;

import java.util.Random;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/home/test")
public class TestController {
    

    @RequestMapping(method=RequestMethod.GET, value="/random/{seed}")
    public Integer getRandom(@PathVariable("seed") long seed) {
        Random rand = new Random(System.currentTimeMillis() * seed);
        return rand.nextInt();
    }
}
