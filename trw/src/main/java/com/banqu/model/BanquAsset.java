package com.banqu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="assets", schema="trw")
public class BanquAsset {
    
    @Id
    @Column(name="assetid")
    String id;
    
    @Column(name="connections_connectionid")
    String ownerId;
    
    //@ManyToOne
    //@JoinColumn(name="connections_connectionid")
    //BanquConnection connection;
    
    @Column(name="asset_balance")
    Float balance;
    
    @Column(name="asset_code")
    String code;
    
    @Column(name="uom")
    String uom;
    
    @Column(name="asset_description")
    String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public Float getBalance() {
        return balance;
    }

    public void setBalance(Float balance) {
        this.balance = balance;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
