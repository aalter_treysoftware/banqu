package com.banqu.model;

import java.util.List;

public class BanquAuthentication {
    
    String id;    
    List<String> roles;
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public List<String> getRoles() {
        return roles;
    }
    public void setRoles(List<String> roles) {
        this.roles = roles;
    }
    
    
}
