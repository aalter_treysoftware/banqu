package com.banqu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="identityproperties", schema="trw")
public class BanquIdentityProperty {

    @Id
    @Column(name="propertyid")
    String id;
    
    @Column(name="property_name")
    String name;
    
    @Column(name="property_value")
    String value;
    
    @ManyToOne
    @JoinColumn(name="identity_identityid")
    BanquIdentity identity;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public BanquIdentity getIdentity() {
        return identity;
    }

    public void setIdentity(BanquIdentity identity) {
        this.identity = identity;
    }
}
