package com.banqu.model;

import java.util.Date;

public class BanquToken {
    
    String token;
    long tokenExpires;
    String refreshToken;
    long refreshTokenExpires;
    
    public String getToken() {
        return token;
    }
    
    public void setToken(String token) {
        this.token = token;
    }
    
    public long getTokenExpires() {
        return tokenExpires;
    }
    
    public Date getTokenExpiresDate() {
        return new Date(tokenExpires);
    }
    
    public void setTokenExpires(long tokenExpires) {
        this.tokenExpires = tokenExpires;
    }
    
    public String getRefreshToken() {
        return refreshToken;
    }
    
    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
    
    public long getRefreshTokenExpires() {
        return refreshTokenExpires;
    }
    
    public void setRefreshTokenExpires(long refreshTokenExpires) {
        this.refreshTokenExpires = refreshTokenExpires;
    }
    
    
}
