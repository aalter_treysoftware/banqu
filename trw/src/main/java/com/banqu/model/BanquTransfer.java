package com.banqu.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="transfers", schema="trw")
public class BanquTransfer {
    
    @Id
    @Column(name="transferid")
    String id;
    
    @ManyToOne
    @JoinColumn(name="assets_assetid")
    BanquAsset asset;
    
    @ManyToOne
    @JoinColumn(name="from_connections_connectionid")
    BanquConnection fromConnection;
    
    @ManyToOne
    @JoinColumn(name="to_connections_connectionid")
    BanquConnection toConnection;
        
    @Column(name="sent_datetime")
    Date sent;
    
    @Column(name="quantity")
    Integer qty;
    
    @Column(name="message")
    String message;
    
    @Column(name="claim_token")
    String claimToken;
    
    @Transient
    String direction;
    
    @Transient
    String uom;
    
    @Transient
    String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BanquAsset getAsset() {
        return asset;
    }

    public void setAsset(BanquAsset asset) {
        this.asset = asset;
    }

    public BanquConnection getFromConnection() {
        return fromConnection;
    }

    public void setFromConnection(BanquConnection fromConnection) {
        this.fromConnection = fromConnection;
    }

    public BanquConnection getToConnection() {
        return toConnection;
    }

    public void setToConnection(BanquConnection toConnection) {
        this.toConnection = toConnection;
    }

    public Date getSent() {
        return sent;
    }

    public void setSent(Date sent) {
        this.sent = sent;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getClaimToken() {
        return claimToken;
    }

    public void setClaimToken(String claimToken) {
        this.claimToken = claimToken;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    
}
    
