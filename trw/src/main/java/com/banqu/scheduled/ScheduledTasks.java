package com.banqu.scheduled;

import java.text.SimpleDateFormat;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.trw.service.TrwAsyncService;

@Component
public class ScheduledTasks {
    
    public static final Log logger = LogFactory.getLog(ScheduledTasks.class);
    
    static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    
    final TrwAsyncService service;
    
    public ScheduledTasks(TrwAsyncService service) {
        this.service = service;
    }

    @Scheduled(cron="${scheduling.job.cron}")
    public void reportCurrentTime() throws InterruptedException, ExecutionException {
        //logger.info("The time is now " + dateFormat.format(new Date()));
        
        long start = System.currentTimeMillis();

        // Kick of multiple, asynchronous lookups
        CompletableFuture<Integer> page1 = service.getRandom(12345);
        CompletableFuture<Integer> page2 = service.getRandom(123456);
        CompletableFuture<Integer> page3 = service.getRandom(1234567);

        // Wait until they are all done
        CompletableFuture.allOf(page1,page2,page3).join();

        // Print results, including elapsed time
        logger.info("Elapsed time: " + (System.currentTimeMillis() - start));
        logger.info("--> " + page1.get());
        logger.info("--> " + page2.get());
        logger.info("--> " + page3.get());
    }    
}
