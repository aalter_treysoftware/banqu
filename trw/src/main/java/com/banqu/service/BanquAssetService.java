package com.banqu.service;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.banqu.model.BanquAsset;
import com.banqu.service.dao.BanquAssetDAO;

@Service
public class BanquAssetService extends BanquService{
    static final Log logger = LogFactory.getLog(BanquAssetService.class);

    @Autowired
    BanquAssetDAO dao;
    
    public BanquAssetService (
            RestTemplateBuilder restTemplateBuilder, 
            @Value("${banqu.default.url}") String urlPrefix,
            @Value("${banqu.default.login}") String banquLogin,
            @Value("${banqu.default.password}") String banquPassword) throws InterruptedException, ExecutionException {
        
        super(restTemplateBuilder, urlPrefix, banquLogin, banquPassword);
    }

    /**
     * 
     * @return
     * @throws InterruptedException
     * @throws ExecutionException
     */
    @Async("threadPoolTaskExecutor")
    public CompletableFuture<List<BanquAsset>> listAssets() throws InterruptedException, ExecutionException {        
        String url = this.urlPrefix + "/assets";
        return CompletableFuture.completedFuture(
                Arrays.asList(
                        this.restTemplate.exchange(
                            url, 
                            HttpMethod.GET, 
                            this.basicHttpEntity(), 
                            BanquAsset[].class
                    )
                    .getBody()
                )
        );
    }
    
    
    public BanquAsset saveAsset(BanquAsset asset) {
        logger.info("Saving asset with id: " + asset.getId());
        return dao.save(asset);
    }
    
    
    
    /***********************************************************************************/
    protected String listAssetsString() {        
        String url = this.urlPrefix + "/assets";
        return this.restTemplate.exchange(
                        url, 
                        HttpMethod.GET, 
                        this.basicHttpEntity(), 
                        String.class
                )
                .getBody();
    }
}
