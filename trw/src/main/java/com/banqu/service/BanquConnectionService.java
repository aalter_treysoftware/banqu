package com.banqu.service;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.banqu.model.BanquConnection;
import com.banqu.service.dao.BanquConnectionDAO;

@Service
public class BanquConnectionService extends BanquService {
    static final Log logger = LogFactory.getLog(BanquConnectionService.class);

    @Autowired
    BanquConnectionDAO dao;

    public BanquConnectionService (
            RestTemplateBuilder restTemplateBuilder, 
            @Value("${banqu.default.url}") String urlPrefix,
            @Value("${banqu.default.login}") String banquLogin,
            @Value("${banqu.default.password}") String banquPassword) throws InterruptedException, ExecutionException {
        
        super(restTemplateBuilder, urlPrefix, banquLogin, banquPassword);
    }
    
    
    /**
     * 
     * @return
     * @throws InterruptedException
     * @throws ExecutionException
     */
    @Async("threadPoolTaskExecutor")
    public CompletableFuture<List<BanquConnection>> listConnections() throws InterruptedException, ExecutionException {        
        String url = this.urlPrefix + "/connections";
        return CompletableFuture.completedFuture(
                Arrays.asList(
                    restTemplate.exchange(
                            url, 
                            HttpMethod.GET, 
                            this.basicHttpEntity(), 
                            BanquConnection[].class
                    )
                    .getBody()
                )
        );
    }    
        
    public void saveAllConnections() throws InterruptedException, ExecutionException {
        this.listConnections().get()
            .forEach(this::saveConnection);
    }
    
    public BanquConnection saveConnection(BanquConnection connection) {
        logger.info("Saving connection with id: " + connection.getId());
        return dao.save(connection);
    }
    
  
    
    
    /******************************************************/
    
    protected String listConnectionsTest() throws InterruptedException, ExecutionException {        
        String url = this.urlPrefix + "/connections";
        return restTemplate.exchange(
                        url, 
                        HttpMethod.GET, 
                        this.basicHttpEntity(), 
                        String.class
                )
                .getBody();
    }    
}
