package com.banqu.service;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.banqu.model.BanquAuthentication;
import com.banqu.model.BanquToken;

@Service
public abstract class BanquService {
    
    final RestTemplate restTemplate;    
    final String urlPrefix;        
    final String banquLogin;    
    final String banquPassword;
    

    BanquToken banquToken;
    
    public BanquService (
            RestTemplateBuilder restTemplateBuilder, 
            @Value("${banqu.default.url}") String urlPrefix,
            @Value("${banqu.default.login}") String banquLogin,
            @Value("${banqu.default.password}") String banquPassword) throws InterruptedException, ExecutionException {
        
        this.restTemplate = restTemplateBuilder.build();
        this.urlPrefix = urlPrefix;
        this.banquLogin = banquLogin;
        this.banquPassword = banquPassword;
        this.banquToken = this.getBanquToken().get();
    }
    
    /**
     * 
     * @return
     * @throws InterruptedException
     */
    @Async("threadPoolTaskExecutor")
    public CompletableFuture<BanquToken> getBanquToken() throws InterruptedException {
        String url = this.urlPrefix + "/auth/token";
        
        JSONObject credentials = new JSONObject();
        credentials.put("login", this.banquLogin);
        credentials.put("password", this.banquPassword);
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<?> entity = new HttpEntity<>(credentials.toString(), headers);
        
        return CompletableFuture.completedFuture(
                restTemplate.postForObject(
                        url, 
                        entity, 
                        BanquToken.class
                )
        );
    }
        
    /**
     * 
     * @return
     */
    HttpEntity<?> basicHttpEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("x-bq-token", this.banquToken.getToken());        
        return new HttpEntity<>(headers); 
    }
    
    /**
     * 
     * @return
     * @throws InterruptedException
     * @throws ExecutionException
     */
    @Async("threadPoolTaskExecutor")
    protected CompletableFuture<BanquAuthentication> getAuthentication() throws InterruptedException, ExecutionException {        
        String url = this.urlPrefix + "/auth";        
        return CompletableFuture.completedFuture(
                restTemplate.exchange(
                        url, 
                        HttpMethod.GET, 
                        this.basicHttpEntity(), 
                        BanquAuthentication.class
                )
                .getBody()
        );
    }
}
