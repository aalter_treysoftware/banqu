package com.banqu.service;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.banqu.model.BanquTransfer;
import com.banqu.service.dao.BanquTransferDAO;

@Service
public class BanquTransferService extends BanquService {
    static final Log logger = LogFactory.getLog(BanquTransferService.class);
    
    @Autowired
    BanquTransferDAO dao;
    
    public BanquTransferService(
            RestTemplateBuilder restTemplateBuilder, 
            @Value("${banqu.default.url}") String urlPrefix,
            @Value("${banqu.default.login}") String banquLogin,
            @Value("${banqu.default.password}") String banquPassword) throws InterruptedException, ExecutionException {
        
        super(restTemplateBuilder, urlPrefix, banquLogin, banquPassword);
    }
    
    /**
     * 
     * @return
     * @throws InterruptedException
     * @throws ExecutionException
     */
    @Async("threadPoolTaskExecutor")
    public CompletableFuture<List<BanquTransfer>> listTransfers() throws InterruptedException, ExecutionException {        
        String url = this.urlPrefix + "/transfers";
        return CompletableFuture.completedFuture(
                Arrays.asList(
                        this.restTemplate.exchange(
                            url, 
                            HttpMethod.GET, 
                            this.basicHttpEntity(), 
                            BanquTransfer[].class
                    )
                    .getBody()
                )
        );
    }
    
    public BanquTransfer saveTransfer(BanquTransfer transfer) {
        logger.info("Saving transfer with id: " + transfer.getId());
        return dao.save(transfer);
    }
    
    /***********************************************************************************/
    protected String listTransfersString() {        
        String url = this.urlPrefix + "/transfers";
        return this.restTemplate.exchange(
                        url, 
                        HttpMethod.GET, 
                        this.basicHttpEntity(), 
                        String.class
                )
                .getBody();
    }

}
