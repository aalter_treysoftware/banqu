package com.banqu.service;

import java.util.concurrent.ExecutionException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

@Service
public class BanquUserProfileService extends BanquService {
    static final Log logger = LogFactory.getLog(BanquUserProfileService.class);
    
    public BanquUserProfileService(
            RestTemplateBuilder restTemplateBuilder, 
            @Value("${banqu.default.url}") String urlPrefix,
            @Value("${banqu.default.login}") String banquLogin,
            @Value("${banqu.default.password}") String banquPassword) throws InterruptedException, ExecutionException {
        
        super(restTemplateBuilder, urlPrefix, banquLogin, banquPassword);
    }

    
    
    
    /***********************************************************************************/
    protected String getProfileString() {        
        String url = this.urlPrefix + "/profile";
        return this.restTemplate.exchange(
                        url, 
                        HttpMethod.GET, 
                        this.basicHttpEntity(), 
                        String.class
                )
                .getBody();
    }

    /*
     * {
          "name": {
            "formatted": "John None Riesenberg",
            "givenName": "John",
            "familyName": "Riesenberg",
            "middleName": "None"
          },
          "addresses": [
            {
              "country": "US",
              "locality": "La Jolla",
              "region": "California",
              "streetAddress": "8060 La Jolla Shores Dr, Suite 7B2"
            }
          ],
          "locale": "en-us",
          "emails": [
            {
              "value": "jriesenberg@treysoftware.com",
              "primary": true
            },
            {
              "bucket": "profile",
              "value": "jriesenberg2@treysoftware.com"
            }
          ],
          "phoneNumbers": [
            {
              "value": "+1 858 220 2852",
              "primary": true
            },
            {
              "bucket": "profile",
              "value": "+1 858 220 2853"
            }
          ],
          "id": "3f36c2fb170146ac8d2477d26d0b3c4a",
          "displayName": "John None Riesenberg",
          "bucket": "profile",
          "photos": [
            {
              "value": "https:\/\/banqu-photos-sandbox.s3.amazonaws.com\/3f36c2fb170146ac8d2477d26d0b3c4a\/f1e4f66a4b5d77b7ffe859c44d6ff300",
              "etag": "e3df9698613d3431c9bcdf6c2b7851c3"
            }
          ]
        }
     */
}
