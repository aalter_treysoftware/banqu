package com.banqu.service.dao;

import org.springframework.data.repository.CrudRepository;

import com.banqu.model.BanquAsset;

public interface BanquAssetDAO extends CrudRepository<BanquAsset, String> {
}
