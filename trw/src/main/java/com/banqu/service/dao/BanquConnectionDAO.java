package com.banqu.service.dao;

import org.springframework.data.repository.CrudRepository;

import com.banqu.model.BanquConnection;

public interface BanquConnectionDAO extends CrudRepository<BanquConnection, String>{
}
