package com.banqu.service.dao;

import org.springframework.data.repository.CrudRepository;

import com.banqu.model.BanquTransfer;

public interface BanquTransferDAO extends CrudRepository<BanquTransfer, Long> {

}
