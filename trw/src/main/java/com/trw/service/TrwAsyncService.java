package com.trw.service;

import java.util.concurrent.CompletableFuture;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class TrwAsyncService {
    
    static final Log logger = LogFactory.getLog(TrwAsyncService.class);
        
    final RestTemplate restTemplate;    

    public TrwAsyncService(RestTemplateBuilder restTemplateBuilder) {
        
        this.restTemplate = restTemplateBuilder.build();
    }
   
    
    @Async("threadPoolTaskExecutor")
    public CompletableFuture<Integer> getRandom(long seed) throws InterruptedException {
        String url = "http://localhost:8090/home/test/random/" + seed;
        Integer results = restTemplate.getForObject(url, Integer.class);
        
        Thread.sleep(2000L);
        
        return CompletableFuture.completedFuture(results);
    }
}
