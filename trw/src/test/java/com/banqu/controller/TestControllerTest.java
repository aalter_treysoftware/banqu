package com.banqu.controller;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.banqu.controller.TestController;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class TestControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;
    
    @Autowired
    TestController testController;
    
    @Test
    public void testGetRandom() {
        long seed = 1235;
        int result = testController.getRandom(seed);
        
        System.out.println(result);
        assertTrue(result != 0);
    }

    @Test
    public void testGetRandomRest(){
        String url = "/home/test/random/12356";
               
        ResponseEntity<Integer> response = restTemplate.getForEntity(url, Integer.class);        
        int result = response.getBody();
        
        System.out.println(result);
        assertTrue(result != 0);
    }
}
