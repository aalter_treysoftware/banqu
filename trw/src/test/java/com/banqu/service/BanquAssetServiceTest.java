package com.banqu.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.concurrent.ExecutionException;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.banqu.model.BanquAsset;
import com.banqu.service.BanquAssetService;
import com.banqu.service.dao.BanquAssetDAO;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("local")
public class BanquAssetServiceTest {

    @Autowired
    BanquAssetService service;
    
    @Autowired
    BanquAssetDAO dao;
    
    @Test
    @Ignore
    public void testListAssets() throws InterruptedException, ExecutionException {
        List<BanquAsset> assets = service.listAssets().get();
        assertNotNull(assets);
        assertTrue(assets.size() > 0);
        System.out.println(assets.get(0).getOwnerId());
    }
    
    @Test
    @Ignore
    public void testListAssetsString() throws InterruptedException, ExecutionException {
        String response = service.listAssetsString();
        System.out.println(response);
    }
    
    @Test
    //@Ignore
    public void testSaveAsset() throws InterruptedException, ExecutionException {
        List<BanquAsset> assets = service.listAssets().get();
        assertNotNull(assets);
        assertTrue(assets.size() > 0);
        
        BanquAsset asset = assets.get(0);
        asset = service.saveAsset(asset);
        
        BanquAsset test = dao.findOne(asset.getId());
        assertEquals(test.getId(), asset.getId());
        
        dao.delete(test);
        
    }

}
