package com.banqu.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.concurrent.ExecutionException;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.banqu.model.BanquConnection;
import com.banqu.service.BanquConnectionService;
import com.banqu.service.dao.BanquConnectionDAO;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("local")
public class BanquConnectionServiceTest {
    
    @Autowired
    BanquConnectionService service;
    
    @Autowired
    BanquConnectionDAO dao;
    
    @Test
    @Ignore
    public void testListConnections() throws InterruptedException, ExecutionException {
        String connections = service.listConnectionsTest();
        System.out.println(connections);
    }
    
    @Test
    //@Ignore
    public void testSaveConnection()  throws InterruptedException, ExecutionException {
        List<BanquConnection> connections = service.listConnections().get();
        BanquConnection result = connections
            .stream()
            .findFirst()
            .map(service::saveConnection)
            .orElse(null);
        
        assertNotNull(result);
        
        BanquConnection test = dao.findOne(result.getId());
        assertEquals(test.getId(), result.getId());
        
        dao.delete(test);
    }
}
