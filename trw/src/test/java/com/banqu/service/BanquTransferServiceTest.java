package com.banqu.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.concurrent.ExecutionException;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.banqu.model.BanquTransfer;
import com.banqu.service.BanquTransferService;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("local")
public class BanquTransferServiceTest {
    
    @Autowired
    BanquTransferService service;

    @Test
    @Ignore
    public void testListTransfers() throws InterruptedException, ExecutionException {
        List<BanquTransfer> transfers  = service.listTransfers().get();
        assertNotNull(transfers);
        assertTrue(transfers.size() > 0);
        System.out.println(transfers.get(0).getId());
    }
    
    @Test
    //@Ignore
    public void testSaveTransfer() throws InterruptedException, ExecutionException {
        BanquTransfer transfer  = service
                .listTransfers()
                .get()
                .stream()
                .findFirst()
                .orElse(null);
        
        assertNotNull(transfer);
        
        service.saveTransfer(transfer);
    }
    
    @Test
    @Ignore
    public void testListTransferString() {
        System.out.println(service.listTransfersString());
    }

}
