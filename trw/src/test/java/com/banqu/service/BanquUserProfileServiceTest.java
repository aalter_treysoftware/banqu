package com.banqu.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.banqu.service.BanquUserProfileService;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("local")
public class BanquUserProfileServiceTest {
    
    @Autowired
    BanquUserProfileService service;

    @Test
    public void testGetProfileString() {
        System.out.println(service.getProfileString());
    }

}
